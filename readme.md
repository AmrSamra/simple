## About
- Simple Laravel app showing my Back-End code sample

## Installation
- clone the repository
- config database in .env file
- composer update
- php artisan migrate --seed

## Unit Testing
- update env file (APP_ENV=testing)
- .\vendor\bin\phpunit