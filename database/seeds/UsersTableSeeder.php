<?php

use Illuminate\Database\Seeder;
use App\User;
use App\Role;

class UsersTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        if(User::count() == 0) {
            foreach (Role::whereIn('slug', ['admin', 'manager'])->cursor() as $role) {
                $role->users()->create([
                    'name'              => $role->name,
                    'email'             => $role->slug . '@simple.com',
                    'email_verified_at' => now(),
                    'password'          => '$2y$10$TKh8H1.PfQx37YgCzwiKb.KjNyWgaHb9cbcoQgdIVFlYg7B77UdFm', // secret
                    'remember_token'    => str_random(10),
                    'role_id'           => $role->id
                ]);
            }
            factory(User::class, 10)->create();
        }
    }
}
