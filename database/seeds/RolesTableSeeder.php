<?php

use Illuminate\Database\Seeder;
use App\Role;

class RolesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        if(Role::count() == 0) {
            $roles = [
                [
                    'name'          => 'Admin',
                    'slug'          => 'admin',
                    'updated_at'    => now(),
                    'created_at'    => now()
                ],
                [
                    'name'          => 'Manager',
                    'slug'          => 'manager',
                    'updated_at'    => now(),
                    'created_at'    => now()
                ],
                [
                    'name'          => 'User',
                    'slug'          => 'user',
                    'updated_at'    => now(),
                    'created_at'    => now()
                ]
            ];
            Role::insert($roles);
        }
    }
}
