<?php

use Illuminate\Database\Seeder;
use App\Car;
use App\User;

class CarsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        if(Car::count() == 0) {
            $cars = [];
            $car = FALSE;
            foreach (User::ByRole('user')->cursor() as $user) {
                if(!$car) {
                    $car = [
                        'model'         => 'MODEL_' . $user->id,
                        'owner_id'      => $user->id,
                        'created_at'    => now(),
                        'updated_at'    => now()
                    ];
                } elseif(is_array($car) && !isset($car['driver_id'])) {
                    $car['driver_id'] = $user->id;
                    $cars[] = $car;
                    $car = FALSE;
                }
            }
            Car::insert($cars);
        }
    }
}
