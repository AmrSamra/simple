<?php

namespace Tests\Unit\Admin;

use Tests\TestCase;
use Illuminate\Foundation\Testing\WithFaker;
use Illuminate\Foundation\Testing\RefreshDatabase;
use App\User;
use App\Role;
use Illuminate\Support\Facades\Session;

class UserTest extends TestCase
{
    private $users;
    private $roles;
    private $user;
    private $admin;
    /**
     * A basic test example.
     *
     * @return void
     */
    public function testRun()
    {
        $this->fillDB();
        $this->setVariables();
        $this->middlewareTest();
        // Set Admin Authentication
        $this->actingAs($this->admin);
        // Controller Testing
        $this->indexTest();
        $this->showTest();
        $this->createTest();
        $this->storeTest();
        $this->editTest();
        $this->updateTest();
        $this->validationRulesTest();
        $this->destroyTest();
        // Clean DB
        $this->cleanDB();
    }

    private function fillDB()
    {
        \Artisan::call('db:seed');
    }

    private function setVariables()
    {
        $this->users = User::with(['role', 'files'])->get();
        $this->roles = Role::pluck('name', 'id')->toArray();
        $this->user = $this->users->where('role_slug', 'user')->first();
        $this->admin = $this->users->where('role_slug', 'admin')->first();
    }

    private function middlewareTest()
    {
        // Guest
        $response = $this->get(route('admin.users.index'));
        $response->assertStatus(302)
            ->assertRedirect(route('login'));

        // Not Admin
        $response = $this->actingAs($this->user)
            ->get(route('admin.users.index'));
        $response->assertStatus(302)
            ->assertRedirect(route('index'));
    }

    private function indexTest()
    {
        $response = $this->get(route('admin.users.index'));
        $response->assertOk()
            ->assertViewIs('admin.index')
            ->assertViewHas('users', $this->users);
    }

    private function showTest()
    {
        $response = $this->get(route('admin.users.show', ['user' => $this->user->id]));
        $response->assertOk()
            ->assertViewIs('admin.show')
            ->assertViewHas('user', $this->user);
    }

    private function createTest()
    {
        $response = $this->get(route('admin.users.create'));
        $response->assertOk()
            ->assertViewIs('admin.edit')
            ->assertViewHas('roles', $this->roles);
    }

    private function storeTest()
    {
        $unique = rand(1000, 9999);
        $role_id = array_search('User' ,$this->roles);
        $response = $this->post(route('admin.users.store'), [
                'name'      => 'test_'.$unique,
                'email'     => 'test_'.$unique.'@test.com',
                'role_id'   => $role_id
            ]);
        // Verify Successes Operation
        $this->assertDatabaseHas('users', [
            'name'      => 'test_'.$unique,
            'email'     => 'test_'.$unique.'@test.com',
            'role_id'   => $role_id
        ]);
        $this->user = User::whereName('test_'.$unique)->first();
        // Assert Redirect After Success
        $response->assertStatus(302)
            ->assertSessionHasNoErrors()
            ->assertRedirect(route('admin.users.show', ['user' => $this->user->id]));
    }

    private function editTest()
    {
        $response = $this->get(route('admin.users.edit', ['user' => $this->user->id]));
        $response->assertOk()
            ->assertViewIs('admin.edit')
            ->assertViewHasAll([
                'roles' => $this->roles,
                'user'  => $this->user
            ]);
    }

    private function updateTest()
    {
        $response = $this->put(route('admin.users.update', ['user' => $this->user->id]), [
                'name'      => $this->user->name.'_update',
                'email'     => $this->user->email,
                'role_id'   => $this->user->role_id
            ]);
        // Verify Successes Operation
        $this->assertDatabaseHas('users', [
            'name'      => $this->user->name.'_update',
            'email'     => $this->user->email,
            'role_id'   => $this->user->role_id
        ]);
        // Assert Redirect After Success
        $response->assertStatus(302)
            ->assertSessionHasNoErrors()
            ->assertRedirect(route('admin.users.show', ['user' => $this->user->id]));
    }

    private function validationRulesTest()
    {
        $response = $this->put(route('admin.users.update', ['user' => $this->user->id]), [
            'name'      => NULL,
            'email'     => 'Not_Valid_Email',
            'role_id'   => 'Not_Integer_Value'
        ]);
        // Verify Fails Operation
        $this->assertDatabaseMissing('users', [
            'email'     => 'Not_Valid_Email',
            'role_id'   => 'Not_Integer_Value'
        ]);
        // Assert Redirect After Failure
        $response->assertStatus(302)
            ->assertSessionHasErrors(['name', 'email', 'role_id'])
            ->assertRedirect(route('admin.users.edit', ['user' => $this->user->id]));
    }

    private function destroyTest()
    {
        $response = $this->delete(route('admin.users.destroy', ['user' => $this->user]));
        // Verify Successes Operation
        $this->assertDatabaseMissing('users', [
            'name'      => $this->user->name.'_update',
            'email'     => $this->user->email,
            'role_id'   => $this->user->role_id
        ]);
        // Assert Redirect After Success
        $response->assertStatus(302)
            ->assertRedirect(route('admin.users.index'));
    }

    private function cleanDB()
    {
        User::where('name', 'like', '%test%')->delete();
    }
}
