<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Role extends Model
{
    protected $table = 'roles';
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = ['name', 'slug'];

    // Define Model Rules
    public static $rules = [
        'name'  => 'required|string|max:100',
        'slug'  => 'required|string|max:100'
    ];

    public function users()
    {
        return $this->hasMany(User::class);
    }
}
