<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use App\Traits\Files;

class Car extends Model
{
    use Files;
    protected $table = 'cars';
    protected $with = ['owner', 'driver', 'files'];

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = ['model', 'owner_id', 'driver_id'];

    // Define Model Rules
    public static $rules = [
        'model'     => 'required|string|max:100',
        'driver_id' => 'nullable|integer|exists:users,id',
        'images.*'  => 'nullable|image'
    ];

    public function owner()
    {
        return $this->belongsTo(User::class, 'owner_id');
    }

    public function driver()
    {
        return $this->belongsTo(User::class, 'driver_id');
    }

    public function getImagesAttribute()
    {
        return $this->getFiles('image');
    }
}
