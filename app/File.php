<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class File extends Model
{
    use SoftDeletes;
    protected $table = 'files';
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'user_id',
        'fileable_id',
        'fileable_type',
        'name',
        'type',
        'path'
    ];

    protected $hidden = [
        'user_id',
        'fileable_id',
        'fileable_type',
        'type'
    ];

    public static $rules = [
        'user_id'       => 'required|integer|exists:users,id',
        'fileable_id'   => 'required|integer',
        'fileable_type' => 'required|string',
        'name'          => 'required|string',
        'type'          => 'required|string',
        'path'          => 'required|string'
    ];

    public function fileable()
    {
        return $this->morphTo();
    }

    public function user()
    {
        return $this->belongsTo(User::class);
    }
}
