<?php
namespace App\Traits;

use App\File;

trait Files
{
    public function files()
    {
        return $this->morphMany(File::class, 'fileable');
    }

    public function uploadFiles($files, $type = 'attachment', $name = FALSE, $folder = FALSE)
    {
        if($files) {
            // Check if One or Multiple Files
            $files = is_array($files) ? $files : [$files];
            foreach ($files as $file) {
                // Store File , return Path
                $savedFile = $file->store($this->getPath($folder), 'public');
                $media[] = [
                    'user_id'   => auth()->user()->id,
                    'name'      => $name ? $name : $file->getClientOriginalName(),
                    'type'      => $type,
                    'path'      => $savedFile
                ];
            }
            if(isset($media) && count($media)) {
                $this->files()->createMany($media);
                // In Success Case
                return TRUE;
            }
        }
        // In Failure Case
        return FALSE;
    }

    public function updateFiles($files, $type = 'attachment', $name = FALSE, $folder = FALSE)
    {
        if($files) {
            // Mention Old First
            $old = $this->getFiles($type);
            // Update New
            if($this->uploadFiles($files, $type, $name, $folder)) {
                // Delete Old
                return $this->deleteFiles($old);
            }
        }
        // In Failure Case
        return FALSE;
    }

    // Get Store Path due to data Table Name For Folders Arrangement
    public function getPath($folder = FALSE)
    {
        $folder = $folder ? $folder : $this->getTable();
        return $folder.'/'.$this->id;
    }

    public function getFiles($type = FALSE, $first = FALSE) {
        // Get Files All or which with the given Type
        $files = $this->files()->when($type, function ($query) use ($type) {
            return $query->where('type', $type);
        });
        // Return First Only If Requested
        if($first) {
            return $files->first();
        }
        return $files->get();
    }

    public Static function deleteFiles($files)
    {
        foreach ($files as $file) {
            $file->delete();
        }
        return TRUE;
    }
}