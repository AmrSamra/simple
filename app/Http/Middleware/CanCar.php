<?php

namespace App\Http\Middleware;

use Closure;
use Auth;
use App\Exceptions\UnAuthorizedAction;

class CanCar
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        if($car = $request->car) {
            $user = Auth::user();
            if( ($user->role_slug == 'user') && ($user->id != $car->owner_id) ) {
                throw new UnAuthorizedAction();
            }
        }
        return $next($request);
    }
}
