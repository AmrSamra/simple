<?php

namespace App\Http\Controllers;

use App\Car;
use Illuminate\Http\Request;
use Auth;
use Lang;
use App\User;
use App\Http\Requests\CarRequest;

class CarController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $cars = [];
        $user = Auth::user();
        if($request->type == 'owner') {
            $cars = $user->CarsAsOwner;
        } elseif ($request->type == 'driver') {
            $cars = $user->CarsAsDriver;
        } else {
            $cars = Car::all();
        }
        return view('main.index', compact(['cars']));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $users = User::pluck('name', 'id')->toArray();
        return view('main.edit', compact(['users']));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(CarRequest $request)
    {
        $data = $request->only(['model', 'driver_id']);
        DB::beginTransaction();
            $car = Auth::user()->CarsAsOwner()->create($data);
            if($images = $request->file('images')) {
                if(! $car->uploadFiles($images, 'image')) {
                    DB::rollBack(); // If Failed RoleBack
                    return back()->withErrors([
                        'message'    => Lang::get('cars.Images Upload Failed')
                    ]);
                }
            }
        DB::commit();
        return redirect()->route('cars.show', ['car' => $car->id]);
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Car  $car
     * @return \Illuminate\Http\Response
     */
    public function show(Car $car)
    {
        return view('main.show', compact(['car']));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Car  $car
     * @return \Illuminate\Http\Response
     */
    public function edit(Car $car)
    {
        $users = User::pluck('name', 'id')->toArray();
        return view('main.edit', compact(['users', 'car']));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Car  $car
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Car $car)
    {
        $data = $request->only(['model', 'driver_id']);
        DB::beginTransaction();
            $car->update($data);
            if($images = $request->file('images')) {
                if(! $car->uploadFiles($images, 'image')) {
                    DB::rollBack(); // If Failed RoleBack
                    return back()->withErrors([
                        'message'    => Lang::get('cars.Images Upload Failed')
                    ]);
                }
            }
        DB::commit();
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Car  $car
     * @return \Illuminate\Http\Response
     */
    public function destroy(Car $car)
    {
        $car->files()->delete();
        $car->delete();
        return back();
    }
}
