<?php

namespace App\Http\Controllers\Admin;

use App\User;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Role;
use App\Http\Requests\Admin\UserRequest;
use DB;
use Lang;

class UserController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $users = User::with(['role', 'files'])->get();
        return view('admin.index', compact(['users']));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $roles = Role::pluck('name', 'id')->toArray();
        return view('admin.edit', compact(['roles']));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(UserRequest $request)
    {
        $data = $request->only(['name', 'email', 'role_id']);
        DB::beginTransaction();
            $data['password'] = isset($data['password']) ? $data['password'] : bcrypt('secret');
            $user = User::create($data);
            if($avatar = $request->file('avatar')) {
                if(! $this->user->uploadFiles($avatar, 'avatar')) {
                    DB::rollBack(); // If Failed RoleBack
                    return back()->withErrors([
                        'message'    => Lang::get('users.Avatar Upload Failed')
                    ]);
                }
            }
        DB::commit();
        return redirect()->route('admin.users.show', ['user' => $user->id]);
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\User  $user
     * @return \Illuminate\Http\Response
     */
    public function show(User $user)
    {
        return view('admin.show', compact(['user']));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\User  $user
     * @return \Illuminate\Http\Response
     */
    public function edit(User $user)
    {
        $roles = Role::pluck('name', 'id')->toArray();
        return view('admin.edit', compact(['roles', 'user']));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\User  $user
     * @return \Illuminate\Http\Response
     */
    public function update(UserRequest $request, User $user)
    {
        $data = $request->only(['name', 'email', 'role_id']);
        DB::beginTransaction();
            $user->update($data);
            if($avatar = $request->file('avatar')) {
                if(! $this->user->updateFiles($avatar, 'avatar')) {
                    DB::rollBack(); // If Failed RoleBack
                    return back()->withErrors([
                        'message'    => Lang::get('users.Avatar Upload Failed')
                    ]);
                }
            }
        DB::commit();
        return redirect()->route('admin.users.show', ['user' => $user->id]);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\User  $user
     * @return \Illuminate\Http\Response
     */
    public function destroy(User $user)
    {
        $user->files()->delete();
        $user->delete();
        return redirect()->route('admin.users.index');
    }
}
