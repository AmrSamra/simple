<?php

namespace App\Http\Requests\Admin;

use App\Http\Requests\BaseRequest;
use App\User;

class UserRequest extends BaseRequest
{
    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        $rules = User::$rules;
        $rules['role_id'] = 'required|integer|exists:roles,id';
        return $rules;
    }
}
