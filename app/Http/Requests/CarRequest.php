<?php

namespace App\Http\Requests;

use App\Car;

class CarRequest extends BaseRequest
{
    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return Car::$rules;
    }
}
