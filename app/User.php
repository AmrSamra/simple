<?php

namespace App;

use Illuminate\Notifications\Notifiable;
use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Foundation\Auth\User as Authenticatable;
use App\Traits\Files;

class User extends Authenticatable
{
    use Notifiable, Files;
    protected $table = 'users';
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name', 'email', 'password', 'role_id'
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token', 'role_id'
    ];

    public static $rules = [
        'name'      => 'required|string|max:100',
        'email'     => 'required|string|email',
        'avatar'    => 'nullable|image'
    ];

    // check is Verified
    public function isVerified()
    {
        if($this->email_verified_at) {
            return TRUE;
        }
        return FALSE;
    }

    // Scope User Where Active By Verification
    public function scopeVerified($query)
    {
        return $query->whereNotNull('email_verified_at');
    }

    public function role()
    {
        return $this->belongsTo(Role::class);
    }

    public function getRoleNameAttribute()
    {
        return $this->role->name;
    }

    public function getRoleSlugAttribute()
    {
        return $this->role->slug;
    }

    // For Check if the user has this role ??
    public function isRole($slug)
    {
        if($this->role_slug == $slug) {
            return TRUE;
        }
        return FALSE;
    }

    // Scope for Query Users By Role
    public function scopeByRole($query, $slug)
    {
        return $query->whereHas('role', function ($sub_query) use ($slug) {
            $sub_query->whereSlug($slug);
        });
    }

    public function CarsAsOwner()
    {
        return $this->hasMany(Car::class, 'owner_id');
    }

    public function CarsAsDriver()
    {
        return $this->hasMany(Car::class, 'driver_id');
    }

    public function getAvatarAttribute()
    {
        $avatar = $this->getFiles('avatar', TRUE);
        return $avatar ? url($avatar->path) : NULL;
    }
}
