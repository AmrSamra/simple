<?php

namespace App\Exceptions;

use Exception;

class UnAuthorizedAction extends Exception
{
    /**
     * @var int
     */
    protected $errorCode = 403;

    /**
     * @param string  $message
     * @param int $errorCode
     */
    public function __construct($message = 'UnAuthorized Action!', $errorCode = null)
    {
        parent::__construct($message);
        if (! is_null($errorCode)) {
            $this->setErrorCode($errorCode);
        }
    }

    /**
     * @param int $errorCode
     */
    public function setErrorCode($errorCode)
    {
        $this->errorCode = $errorCode;
    }

    /**
     * @return int the status code
     */
    public function getErrorCode()
    {
        return $this->errorCode;
    }
}
