<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
})->name('index');

Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');

Route::group(['middleware' => ['web', 'auth']], function () {
    // Admin Routes
    Route::group(['prefix' => 'admin', 'middleware' => ['admin'], 'as' => 'admin.', 'namespace' => 'Admin'], function () {
        Route::resource('users', 'UserController');
    });
    // Main Routes
    Route::resource('cars', 'CarController')->middleware('can.car');
});
